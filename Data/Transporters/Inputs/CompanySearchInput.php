<?php

namespace App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs;

class CompanySearchInput
{
    /** @var string */
    private $query = "";

    /** @var int id поля по которому будет производиться поиск */
    private $fieldId = 0;

    /** @var bool поиск по кастомному полю */
    private $searchByCustomField = false;

    public function __construct(string $query)
    {
        $this->query = $query;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function searchInCustomFieldIsEmbedded(): bool
    {
        return $this->searchByCustomField;
    }

    public function getCustomSearchFieldId(): int
    {
        return $this->fieldId;
    }

    public function searchIn(int $fieldId): self
    {
        $this->searchByCustomField = true;
        $this->fieldId = $fieldId;

        return $this;
    }
}