<?php

namespace App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs;

class CompanyEditInput
{
    private $id = 0;
    private $okved = "";

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOkved(): string
    {
        return $this->okved ?: "";
    }

    /**
     * @param string $okved ОКВЭД
     */
    public function setOkved(string $okved): self
    {
        $this->okved = $okved;
        return $this;
    }
}