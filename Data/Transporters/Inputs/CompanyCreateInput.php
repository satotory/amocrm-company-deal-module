<?php

namespace App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs;

class CompanyCreateInput
{
    /** @var string */
    private $name = "";

    /** @var string */
    private $phone = "";

    /** @var string */
    private $email = "";

    /** @var string */
    private $okved = "";

    /** @var int */
    private $INN = "";

    /** @var string */
    private $yrAddress = "";

    /** @var int */
    private $responsibleId = 0;
    
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getYrAddress(): string
    {
        return $this->yrAddress;
    }

    public function setYrAddress(string $yrAddress): self
    {
        $this->yrAddress = $yrAddress;

        return $this;
    }

    public function getOkved(): string
    {
        return $this->okved;
    }

    public function setOkved(string $okved): self
    {
        $this->okved = $okved;

        return $this;
    }

    public function getINN(): int
    {
        return $this->INN;
    }

    public function setINN(int $INN): self
    {
        $this->INN = $INN;

        return $this;
    }

    public function getResponsibleId(): int
    {
        return $this->responsibleId;
    }

    public function setResponsibleId(int $responsibleId): self
    {
        $this->responsibleId = $responsibleId;

        return $this;
    }
}