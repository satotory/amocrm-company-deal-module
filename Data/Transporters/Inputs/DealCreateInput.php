<?php

namespace App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs;

class DealCreateInput
{
    /** @var string */
    private $phone = "";

    /** @var string */
    private $email = "";

    /** @var string */
    private $title = "";

    /** @var string */
    private $address = "";

    /** @var string */
    private $tag = "";

    /** @var string */
    private $companyName = "";

    /** @var float */
    private $amount = 0.0;

    /** @var int */
    private $companyId = 0;
    
    /** @var int */
    private $responsibleId = 0;

    public function __construct(string $title, string $tag)
    {
        $this->title = $title;
        $this->tag = $tag;
    }

    public function getPhone(): string
    {
        return $this->phone ?: "";
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email ?: "";
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title ?: "";
    }

    public function getAddress(): string
    {
        return $this->address ?: "";
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getResponsibleId(): int
    {
        return $this->responsibleId;
    }

    public function setResponsibleId(int $responsibleId): self
    {
        $this->responsibleId = $responsibleId;

        return $this;
    }

    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    public function setCompanyId(int $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function getTag(): string
    {
        return $this->tag ?: "";
    }

	public function getAmount() 
    {
		return $this->amount ?: 0.0;
	}
	
	public function setAmount(float $amount): self 
    {
		$this->amount = $amount;
		return $this;
	}
}