<?php

namespace App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs;

class CompanyGetInfoInput
{
    /** @var int */
    private $searchId = 0;

    public function __construct(int $searchId)
    {
        $this->searchId = $searchId;
    }

    public function getCompanyId(): int
    {
        return $this->searchId;
    }
}