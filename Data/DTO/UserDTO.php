<?php

namespace App\modules\Korzilla\AmoCRM\Data\DTO;

class UserDTO
{    
    /**
     * ID пользователя
     *
     * @var int
     */
    public $id;
    
    /**
     * Полное имя пользователя
     *
     * @var string
     */
    public $name;
    
    /**
     * E-mail пользователя
     *
     * @var string
     */
    public $email;
    
    /**
     * Язык пользователя. Один из вариантов: ru, en, es
     *
     * @var string
     */
    public $lang = 'ru';
    
    /**
     * Права пользователя
     *
     * @var UserRightsDTO
     */
    public $rights;
}

class UserRightsDTO
{    
    /**
     * Объект прав доступа к сделкам
     *
     * @var array
     */
    public $leads = [];
    
    /**
     * Объект прав доступа к контактам
     *
     * @var array
     */
    public $contacts = [];
    
    /**
     * Объект прав доступа к компаниям
     *
     * @var array
     */
    public $companies = [];
    
    /**
     * Объект прав доступа к задачам
     *
     * @var array
     */
    public $tasks = [];
    
    /**
     * Доступ к функционалу почты.
     *
     * @var bool
     */
    public $mail_access = false;
    
    /**
     * Доступ к функционалу списков.
     *
     * @var bool
     */
    public $catalog_access = false;
    
    /**
     * Доступ к функционалу файлов.
     *
     * @var bool
     */
    public $files_access = false;
    
    /**
     * Массив из объектов, которые описывают права на статусы
     *
     * @var array
     */
    public $status_rights = [];
    
    /**
     * Является ли пользователь администратором
     *
     * @var bool
     */
    public $is_admin;
    
    /**
     * Является ли пользователь бесплатным
     *
     * @var bool
     */
    public $is_free;
    
    /**
     * Является ли пользователь активным
     *
     * @var bool
     */
    public $is_active;
    
    /**
     * ID группы, к которой относится пользователь
     *
     * @var int|null
     */
    public $group_id = null;
    
    /**
     * ID роли, которая установлена у пользователя
     *
     * @var int|null
     */
    public $role_id = null;
}
