<?php

namespace App\modules\Korzilla\AmoCRM\Data\DTO;

class CompanyDTO
{
    public $id;
    public $name;
    public $responsibleID;
}