<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Exceptions\TokenUnexpectedException;

class TokenGetNewTask extends CurlTask
{
    private const METHOD = "oauth2/access_token";
    private const GRANT_TYPE = 'refresh_token';

    protected function getMethod(): string
    {
        return self::METHOD;
    }

    public function run()
    {
        global $setting;
        
        $data = [
			'grant_type' => self::GRANT_TYPE,
			'client_id' =>  $this->config->getClientId(),
			'client_secret' => $this->config->getClientSecret(),
			'refresh_token' => $this->config->getRefreshToken(),
			'redirect_uri' => $this->config->getRedirectUri()
		];

		$response = $this->curlPost($data);

        if (!$response) {
            throw new TokenUnexpectedException();
        }

        $data = json_decode($response, 1);
        
		if (!$data['access_token']) {
            throw new TokenUnexpectedException();
        }
        
        $setting['actoken'] = $data['access_token']; //Access токен
        $setting['acreftoken'] = $data['refresh_token']; //Refresh токен
        $setting['actime'] = date("Y-m-d") .' по '. date("Y-m-d", time() + $data['expires_in']);
        $setting['actimestamp'] = time() + $data['expires_in']; //Время жизни токена

        setSettings($setting);

        $this->config->setToken($setting['actoken']);
        $this->config->setRefreshToken($setting['acreftoken']);
        $this->config->setTokenTimestamp($setting['actimestamp']);
    }
}