<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\CompanyDTO;
use App\modules\Korzilla\AmoCRM\Exceptions\CompanyGetInfoUnexpectedError;

class CompanyGetInfoTask extends CurlTask
{
    private const METHOD = "api/v4/companies/%d";
    private $companyId;

    protected function getMethod(): string
    {
        return sprintf(self::METHOD, $this->companyId);
    }

    /**
     * @throws CompanyGetInfoUnexpectedError
     */
    public function run(int $companyId): CompanyDTO
    {
        $this->companyId = $companyId;

        $response = $this->curlGet();
        if (!$response) {
            throw new CompanyGetInfoUnexpectedError;
        }

        $data = json_decode($response, 1);

        $companyDTO = new CompanyDTO;

        $companyDTO->id = $data['id'];
        $companyDTO->responsibleID = $data['responsible_user_id'];

        return $companyDTO;
    }
}