<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\DealDTO;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\DealCreateInput;
use App\modules\Korzilla\AmoCRM\Exceptions\DealCreatingUnexpectedError;

class DealCreateTask extends CurlTask
{
    private const METHOD = "api/v4/leads/complex";

    protected function getMethod(): string
    {
        return self::METHOD;
    }

    public function run(DealCreateInput $input)
    {
        $data = [[
            "name" => $input->getTitle(),
            "created_at" => time(),
            "created_by" => 0,
            "responsible_user_id" => (int) $input->getResponsibleId() ?: 0,
            "_embedded" => [
                "tags" => [
                    [ "name" => $input->getTag(), ],
                ],
                "companies" => [
                    [ "id" => $input->getCompanyId(), ]
                ],
            ],
        ]];

        if ($input->getAmount()) {
            $data[0]["price"] = (int) round($input->getAmount());
        }

        $response = $this->curlPost($data);
        
        if (!$response) {
            throw new DealCreatingUnexpectedError();
        }

        $data = json_decode($response, 1);

        if (!$data[0]['id']) {
            throw new DealCreatingUnexpectedError();
        }
        
        $output = new DealDTO;
        $output->id = (int) $data[0]['id'];

        return $output;
    }
}