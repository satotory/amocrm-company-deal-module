<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\CompanyDTO;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyCreateInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CompanyCreatingUnexpectedError;

class CompanyCreateTask extends CurlTask
{
    private const METHOD = "api/v4/companies";

    private $customFields = [];

    protected function getMethod(): string
    {
        return self::METHOD;
    }

    public function run(CompanyCreateInput $input)
    {
        $data = [[
            "name" => $input->getName(),
            "custom_fields_values" => [
                [ 
                    "field_code" => "PHONE",
                    "values" => [
                        ["value" => $input->getPhone() ?: "",
                        "enum_code" => "WORK"]
                    ]
                ],
                [
                    "field_code" => "EMAIL",
                    "values" => [
                        ["value" => $input->getEmail() ?: "",
                        "enum_code" => "WORK"]
                    ]
                ],
            ],
        ]];

        if ($responsibleId = $input->getResponsibleId()) {
            $data[0]['responsible_user_id'] = $responsibleId;
        }

        $this->customFields = &$data[0]['custom_fields_values'];

        if ($this->config->okvedIsEmbedded()) {
            $this->setCustomField(
                $this->config->getOkvedFieldID(),
                $input->getOkved()
            );
        }

        if ($this->config->innIsEmbedded()) {
            $this->setCustomField(
                $this->config->getInnFieldID(),
                $input->getINN()
            );
        }

        if ($this->config->yrAddressIsEmbedded()) {
            $this->setCustomField(
                $this->config->getYrAddressFieldID(),
                $input->getYrAddress()
            );
        }

        $response = $this->curlPost($data);
        
        if (!$response) {
            throw new CompanyCreatingUnexpectedError();
        }

        $data = json_decode($response, 1);

        if (!$data['_embedded']['companies'][0]['id']) {
            throw new CompanyCreatingUnexpectedError();
        }

        $output = new CompanyDTO;
        $output->id = $data['_embedded']['companies'][0]['id'];
        $output->name = $input->getName();
        
        return $output;
    }

    private function setCustomField(int $field_id, $value)
    {
        $this->customFields[] = [
            "field_id" => $field_id,
            "values" => [
                [
                    "value" => (string) $value ?: ""
                ],
            ]
        ];
    }
}