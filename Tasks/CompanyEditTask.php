<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\CompanyDTO;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyEditInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CompanyEditUnexpectedError;

class CompanyEditTask extends CurlTask
{
    private const METHOD = "api/v4/companies/%d";
    private $companyId;

    protected function getMethod(): string
    {
        return sprintf(self::METHOD, $this->companyId);
    }

    public function run(CompanyEditInput $input)
    {
        $this->companyId = $input->getId();

        $data = [
            "id" => $input->getId(),
        ];

        if ($this->config->okvedIsEmbedded()) {
            $data["custom_fields_values"] = [
                [ 
                    "field_id" => $this->config->getOkvedFieldID(),
                    "values" => [
                        [
                            "value" => $input->getOkved()
                        ],
                    ]
                ],
            ];
        }

        $response = $this->curlPatch($data);
        
        if (!$response) {
            throw new CompanyEditUnexpectedError();
        }

        $data = json_decode($response, 1);
        
        if (!$data['_embedded']['companies'][0]['id']) {
            throw new CompanyEditUnexpectedError();
        }

        $output = new CompanyDTO;
        $output->id = $data['_embedded']['companies'][0]['id'];
        
        return $output;
    }
}