<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;

abstract class CurlTask
{    
    /** @var AmoCRMConfig */
    protected $config;

    private const PROTOCOL = 'https';
    private const HOST = 'amocrm.ru';
    private const USER_AGENT = 'amoCRM-oAuth-client/1.0';

    public function __construct(AmoCRMConfig $config)
    {
        $this->config = $config;
    }

    abstract protected function getMethod() : string;

    protected function curlGet(array $getParams = [])
    {
        $url = $this->getUrl();

        if ($getParams) {
            $query = http_build_query($getParams, "", "&");
            $url .= "?". $query;
        }

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_HTTPHEADER => $this->getHeaders(),
            CURLOPT_CUSTOMREQUEST => 'GET',
        ]);

        $reply = curl_exec($ch);
        curl_close($ch);

        return $reply;
    }

    protected function curlPost(array $postParams)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_URL => $this->getUrl(),
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_HTTPHEADER => $this->getHeaders(),
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($postParams),
        ]);

        $reply = curl_exec($ch);
        curl_close($ch);

        return $reply;
    }

    protected function curlPatch(array $postParams)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_URL => $this->getUrl(),
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_HTTPHEADER => $this->getHeaders(),
            CURLOPT_CUSTOMREQUEST => 'PATCH',
            CURLOPT_POSTFIELDS => json_encode($postParams),
        ]);

        $reply = curl_exec($ch);
        curl_close($ch);

        return $reply;
    }

    private function getUserAgent()
    {
        return self::USER_AGENT;
    }

    private function getUrl()
    {
        return sprintf(
            "%s://%s.%s/%s",
            self::PROTOCOL,
            $this->config->getDomain(),
            self::HOST,
            $this->getMethod()
        );
    }

    private function getHeaders()
    {
        return [
            'Authorization: Bearer '. $this->config->getToken(),
            'Content-Type: application/json'
        ];
    }
}
