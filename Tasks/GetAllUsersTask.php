<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\UserDTO;
use App\modules\Korzilla\AmoCRM\Data\DTO\UserRightsDTO;

class GetAllUsersTask extends CurlTask
{
    private const METHOD = 'api/v4/users';
    
    protected function getMethod(): string
    {
        return self::METHOD;
    }

    /** @return UserRightsDTO[] */
    public function run(): array
    {
        $users = [];

        $response = json_decode($this->curlGet(), 1);
        if (!isset($response['_embedded']['users'])) return $users;

        foreach ($response['_embedded']['users'] as $user) {
            $userDto = new UserDTO;
            $userDto->id = $user['id'];
            $userDto->name = $user['name'];
            $userDto->email = $user['email'];
            $userDto->lang = $user['lang'];

            $userDto->rights = new UserRightsDTO;
            $userDto->rights->leads = $user['rights']['leads'];
            $userDto->rights->contacts = $user['rights']['contacts'];
            $userDto->rights->companies = $user['rights']['companies'];
            $userDto->rights->tasks = $user['rights']['tasks'];
            $userDto->rights->mail_access = $user['rights']['mail_access'];
            $userDto->rights->catalog_access = $user['rights']['catalog_access'];
            $userDto->rights->status_rights = $user['rights']['status_rights'];
            $userDto->rights->is_admin = $user['rights']['is_admin'];
            $userDto->rights->is_free = $user['rights']['is_free'];
            $userDto->rights->is_active = $user['rights']['is_active'];
            $userDto->rights->group_id = $user['rights']['group_id'];
            $userDto->rights->role_id = $user['rights']['role_id'];

            $users[] = $userDto;
        }
        return $users;
    }
}
