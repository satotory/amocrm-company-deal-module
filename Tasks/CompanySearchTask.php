<?php

namespace App\modules\Korzilla\AmoCRM\Tasks;

use App\modules\Korzilla\AmoCRM\Data\DTO\CompanyDTO;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanySearchInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CompanyNotFoundException;

class CompanySearchTask extends CurlTask
{
    private const METHOD = "api/v4/companies";
    
    protected function getMethod(): string
    {
        return self::METHOD;
    }

    public function run(CompanySearchInput $input)
    {
        $query = $this->clearQuery($input->getQuery());

        $getParams = [
            "query" => $query,
            "order" => [
                "id" => "asc",
            ]
        ];

        $response = $this->curlGet($getParams);

        if (!$response) {
            throw new CompanyNotFoundException();
        }

        $data = json_decode($response, 1);
        $companies = &$data['_embedded']['companies'];
        
        if ($input->searchInCustomFieldIsEmbedded()) {
            $companyFounded = $this->searchInField($companies, $query, $input->getCustomSearchFieldId());
        } else {
            $companyFounded = $this->casualSearch($companies, $query);
        }

        if (!$companyFounded) {
            throw new CompanyNotFoundException();
        }

        $output = new CompanyDTO;

        $output->id = (int) $companyFounded['id'];
        $output->name = (string) $companyFounded['name'];
        $output->responsibleID = (int) $companyFounded['responsible_user_id'];

        return $output;
    }

    private function searchInField(array $companies, string $search, int $searchFieldId)
    {
        $companyFounded = NULL;

        foreach ($companies as $company) {
            if ($company['is_deleted'] || !isset($company['custom_fields_values']))
                continue;

            foreach ($company['custom_fields_values'] as $field) {
                if ($field['field_id'] != $searchFieldId)
                    continue;
                
                if (!isset($field['values']))
                    continue;

                foreach ($field['values'] as $value) {
                    if ($value['value'] == $search) {
                        $companyFounded = $company;
                        break;
                    }
                }
            }
        }

        return $companyFounded;
    }

    private function casualSearch(array $companies, string $search)
    {
        $companyFounded = NULL;

        foreach ($companies as $company) {
            if ($company['is_deleted'])
                continue;

            if ($company['name'] == $search) {
                $companyFounded = $company;
                break;
            }

            if (!isset($company['custom_fields_values']))
                continue;

            foreach ($company['custom_fields_values'] as $field) {
                if (!isset($field['values']))
                    continue;

                foreach ($field['values'] as $value) {
                    if ($value['value'] == $search) {
                        $companyFounded = $company;
                        break;
                    }
                }
            }
        }

        return $companyFounded;
    }

    private function clearQuery(string $query)
    {
        return str_replace("\n", '', str_replace("\r", '', $query));
    }
}