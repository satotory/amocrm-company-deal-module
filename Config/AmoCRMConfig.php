<?php

namespace App\modules\Korzilla\AmoCRM\Config;

class AmoCRMConfig
{
    /** @var string */
    private $domain;

    /** @var string */
    private $token;

    /** @var string */
    private $clientId;
    
    /** @var string */
    private $clientSecret;
    
    /** @var string */
    private $refreshToken;
    
    /** @var string */
    private $redirectURI;

    /** @var int */
    private $tokenTimestamp;

    /** @var OKVEDConfig */
    private $okvedField;

    /** @var INNConfig */
    private $innField;

    /** @var YrAddressConfig */
    private $yrAddressField;

    public function __construct(
        string $domain, 
        string $token, 
        string $clientId, 
        string $clientSecret, 
        string $refreshToken, 
        string $redirectURI, 
        int $tokenTimestamp
    ) {
        $this->domain = $domain;
        $this->token = $token;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->refreshToken = $refreshToken;
        $this->redirectURI = $redirectURI;
        $this->tokenTimestamp = $tokenTimestamp;

        $this->okvedField = new OKVEDConfig;
        $this->innField = new INNConfig;
        $this->yrAddressField = new YrAddressConfig;
    }

    public function setINN(int $field_id): self
    {
        $this->innField->included = true;
        $this->innField->field_id = $field_id;

        return $this;
    }

    public function setOkved(int $field_id): self
    {
        $this->okvedField->included = true;
        $this->okvedField->field_id = $field_id;

        return $this;
    }

    public function setYrAddress(int $field_id): self
    {
        $this->yrAddressField->included = true;
        $this->yrAddressField->field_id = $field_id;

        return $this;
    }

    public function okvedIsEmbedded(): bool
    {
        return $this->okvedField->included;
    }

    public function innIsEmbedded(): bool
    {
        return $this->innField->included;
    }

    public function yrAddressIsEmbedded(): bool
    {
        return $this->yrAddressField->included;
    }

    public function getOkvedFieldID(): int
    {
        return $this->okvedField->field_id;
    }

    public function getInnFieldID(): int
    {
        return $this->innField->field_id;
    }

    public function getYrAddressFieldID(): int
    {
        return $this->yrAddressField->field_id;
    }

    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getRedirectUri(): string
    {
        return $this->redirectURI;
    }

    public function setRedirectUri(string $redirectURI): self
    {
        $this->redirectURI = $redirectURI;

        return $this;
    }

    
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function setClientSecret(string $clientSecret): self
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function setTokenTimestamp(int $tokenTimestamp): self
    {
        $this->tokenTimestamp = $tokenTimestamp;

        return $this;
    }

    public function getTokenTimestamp(): int
    {
        return $this->tokenTimestamp;
    }

	public function getToken(): string
    {
		return $this->token;
	}
	
	public function setToken(string $token): self
    {
		$this->token = $token;

		return $this;
	}
}

class OKVEDConfig extends EmbeddedFieldConfig {}

class INNConfig extends EmbeddedFieldConfig {}

class YrAddressConfig extends EmbeddedFieldConfig {}

abstract class EmbeddedFieldConfig
{
    /** @var bool */
    public $included = false;
    /** @var int */
    public $field_id = NULL;
}