<?php
 
require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

global $setting;

// внешние данные
$customf['phone']['value'] = "89063334103";
$customf['email']['value'] = "email@email.email";
// $customf['company']['value'] = "Фамилия И.О. Покупателя " . rand(200, 1000); 
$customf['company']['value'] = "Фамилия И.О. Покупателя 16"; 
$customf['okved']['value'] = "ОКВЭД";
$customf['address']['value'] = "Адрес";
$customf['inn']['value'] = rand(6040500003, 9040500003);
$order['totalsum'] = rand(100, 4000);
$f_fio = "ФИО покупателя" . rand(33, 99);
$message = rand(0, 2500);

// работа с контейнером
$companyData = [
    "name" => $customf['company']['value'],
    "okved" => $customf['okved']['value'],
    "address" => $customf['address']['value'],
    "phone" => $customf['phone']['value'],
    "email" => $customf['email']['value'],
    "inn" => $customf['inn']['value'],
];

// если из вне задать ид ответственного, но компания уже будет существовать с другим ответственным, то у сделки будет заданный ответственный, а в компании останется старый ответственный
$responsibleId = 9910046;

$dealData = [
    "amount" => $order['totalsum'],
    "name" => $f_fio,
    "title" => "Заказ №" . $message,
    "tag" => 'Заказ с сайта',
];

// создаем конфиг
$config = \App\modules\Korzilla\AmoCRM\Factories\AmoCRMConfigFactory::get($setting);
// создаем сервис, через который будет происходить вызов действий
$service = new \App\modules\Korzilla\AmoCRM\Service\AmoCRMService($config);

// ОПЦИОНАЛЬНО:
// можно забрать список всех пользователей AmoCRM $users = $service->getAllUsers();

// создаем инпут для поиска компании, задаем что искать в __construct
$companySearchInput = (new \App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanySearchInput($companyData['name']));

// ищем существует ли компания
$company = $service->companySearch($companySearchInput);
if ($company->id) {
    // ОПЦИОНАЛЬНО: 
    // если компания существует, можем отредактировать ее
    // создаем инпут для редактирования, с указыванием id компании в __construct
    $companyEditInput = (new \App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyEditInput($company->id))
        ->setOkved($companyData['okved']);
    
    // вызываем действие на редактирование компании
    $service->companyEdit($companyEditInput);
} else {
    // если компании нет, ее нужно создать
    // создаем инпут для создания компании, задав имя компании в __construct
    $companyCreateInput = (new \App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyCreateInput($companyData['name']))
        // ОПЦИОНАЛЬНО: задаем нужные поля
        ->setPhone($companyData['phone'])
        ->setEmail($companyData['email'])
        ->setOkved($companyData['okved'])
        ->setINN($companyData['inn'])
        ->setYrAddress($companyData['address']);

    if ($responsibleId) {
        // если у нас уже есть информация об id ответственного, при создании можно задать компании id ответственного
        $companyCreateInput->setResponsibleId($responsibleId);
    }

    // вызываем действие на создание компании
    $company = $service->companyCreate($companyCreateInput);
}

if (!$responsibleId) {
    // ОПЦИОНАЛЬНО:
    // для установки нужного id ответственного в сделку, нужно узнать какой ответственный указан у компании
    // создаем инпут для выборки информации о компании
    $companyGetInfoInput = new \App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyGetInfoInput($company->id);
    // вызываем действие на выборку информации о компании
    $companyInfo = $service->getCompanyInfo($companyGetInfoInput);

    // присваиваем id ответственного
    $responsibleId = $companyInfo->responsibleID;
}

// создаем инпут для создания сделки, указывая название сделки и тег сделки в __construct
$dealCreateInput = (new \App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\DealCreateInput($dealData['title'], $dealData['tag']))
    // ОПЦИОНАЛЬНО: задаем нужные поля
    ->setAmount((float) $dealData['amount'])
    ->setCompanyId($company->id)
    ->setResponsibleId($responsibleId);

// вызываем действие для создания сделки
$deal = $service->dealCreate($dealCreateInput);