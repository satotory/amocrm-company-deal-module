<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;

class AmoCRMConfigFactory
{
    private static $instance;

    public static function get(array $setting): AmoCRMConfig
    {
        if (!self::$instance) {
            self::$instance = self::create($setting);
        }

        return self::$instance;
    }

    private static function create(array $setting) : AmoCRMConfig
    {
        $config = new AmoCRMConfig(
            (string) $setting['acsubdomen'],
            (string) $setting['actoken'],
            (string) $setting['acclientid'],
            (string) $setting['acclientsecret'],
            (string) $setting['acreftoken'],
            ($_SERVER['HTTPS'] == 'on' ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}/",
            (string) $setting['actimestamp']
        );
        
        if (self::valueIsSet($setting, 'ac_okved_fid')) {
            $config->setOkved($setting['ac_okved_fid']);
        }

        if (self::valueIsSet($setting, 'ac_inn_fid')) {
            $config->setINN($setting['ac_inn_fid']);
        }

        if (self::valueIsSet($setting, 'ac_yr_address_fid')) {
            $config->setYrAddress($setting['ac_yr_address_fid']);
        }

        return $config;
    }

    private static function valueIsSet(array $array, string $key): bool
    {
        return isset($array[$key]) && !empty($array[$key]);
    }
}