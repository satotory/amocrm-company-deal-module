<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\GetAllUsersAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\GetAllUsersTask;

class GetAllUsersFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): GetAllUsersAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : GetAllUsersAction
    {
        $getAllUsersTask = new GetAllUsersTask($config);

        return new GetAllUsersAction($getAllUsersTask);
    }
}
