<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\CompanyEditAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyEditTask;

class CompanyEditActionFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): CompanyEditAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : CompanyEditAction
    {
        $companyCreateTask = new CompanyEditTask($config);

        return new CompanyEditAction($companyCreateTask);
    }
}