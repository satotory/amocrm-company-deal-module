<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\CompanyGetInfoAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyGetInfoTask;

class CompanyGetInfoActionFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): CompanyGetInfoAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : CompanyGetInfoAction
    {
        $dealCreateTask = new CompanyGetInfoTask($config);

        return new CompanyGetInfoAction($dealCreateTask);
    }
}