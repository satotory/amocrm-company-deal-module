<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\CompanySearchAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\CompanySearchTask;

class CompanySearchActionFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): CompanySearchAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : CompanySearchAction
    {
        $companySearchTask = new CompanySearchTask($config);

        return new CompanySearchAction(
            $companySearchTask
        );
    }
}