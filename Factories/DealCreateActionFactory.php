<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\DealCreateAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\DealCreateTask;

class DealCreateActionFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): DealCreateAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : DealCreateAction
    {
        $dealCreateTask = new DealCreateTask($config);

        return new DealCreateAction($dealCreateTask);
    }
}