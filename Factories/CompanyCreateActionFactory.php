<?php

namespace App\modules\Korzilla\AmoCRM\Factories;

use App\modules\Korzilla\AmoCRM\Actions\CompanyCreateAction;
use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyCreateTask;

class CompanyCreateActionFactory
{
    private static $instance;

    public static function get(AmoCRMConfig $config): CompanyCreateAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(AmoCRMConfig $config) : CompanyCreateAction
    {
        $companyCreateTask = new CompanyCreateTask($config);

        return new CompanyCreateAction($companyCreateTask);
    }
}