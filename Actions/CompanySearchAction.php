<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanySearchInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CriticalException;
use App\modules\Korzilla\AmoCRM\Tasks\CompanySearchTask;

class CompanySearchAction
{
    /** @var CompanySearchTask */
    private $task;

    public function __construct(
        CompanySearchTask $task
    ) {
        $this->task = $task;
    }

    public function run(CompanySearchInput $input)
    {
        try {
            $output = $this->task->run($input);
        } catch (CriticalException $e) {
            $output = $e->getMessage();
        }

        return $output;
    }
}