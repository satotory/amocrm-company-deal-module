<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyEditInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CriticalException;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyEditTask;

class CompanyEditAction
{
    /** @var CompanyEditTask */
    private $task;

    public function __construct(
        CompanyEditTask $task
    ) {
        $this->task = $task;
    }

    public function run(CompanyEditInput $input)
    {
        try {
            $output = $this->task->run($input);
        } catch (CriticalException $e) {
            $output = $e->getMessage();
        }

        return $output;
    }
}