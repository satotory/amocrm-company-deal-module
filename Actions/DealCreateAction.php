<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\DealCreateInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CriticalException;
use App\modules\Korzilla\AmoCRM\Tasks\DealCreateTask;

class DealCreateAction
{
    /** @var DealCreateTask */
    private $task;

    public function __construct(
        DealCreateTask $task
    ) {
        $this->task = $task;
    }

    public function run(DealCreateInput $input)
    {
        try {
            $output = $this->task->run($input);
        } catch (CriticalException $e) {
            $output = $e->getMessage();
        }

        return $output;
    }
}