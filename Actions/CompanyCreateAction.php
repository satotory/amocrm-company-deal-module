<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyCreateInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CriticalException;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyCreateTask;

class CompanyCreateAction
{
    /** @var */
    private $task;
    private $customFields;
    private $input;

    public function __construct(
        CompanyCreateTask $task
    ) {
        $this->task = $task;
    }

    public function run(CompanyCreateInput $input)
    {
        $this->input = $input;

        try {
            $output = $this->task->run($input);
        } catch (CriticalException $e) {
            $output = $e->getMessage();
        }

        return $output;
    }

}