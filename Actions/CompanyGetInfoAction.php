<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyGetInfoInput;
use App\modules\Korzilla\AmoCRM\Exceptions\CriticalException;
use App\modules\Korzilla\AmoCRM\Tasks\CompanyGetInfoTask;

class CompanyGetInfoAction
{
    /** @var CompanyGetInfoTask */
    private $companyGetInfoTask;

    public function __construct(CompanyGetInfoTask $companyGetInfoTask) 
    {
        $this->companyGetInfoTask = $companyGetInfoTask;
    }

    public function run(CompanyGetInfoInput $input)
    {
        try {
            $output = $this->companyGetInfoTask->run($input->getCompanyId());
        } catch (CriticalException $e) {
            $output = $e->getMessage();
        }

        return $output;
    }
}