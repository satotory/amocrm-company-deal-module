<?php

namespace App\modules\Korzilla\AmoCRM\Actions;

use App\modules\Korzilla\AmoCRM\Tasks\GetAllUsersTask;

class GetAllUsersAction
{    
    /** @var GetAllUsersTask */
    private $task;
    
    public function __construct(GetAllUsersTask $task)
    {
        $this->task = $task;
    }
    
    /** @return UserRightsDTO[] */
    public function run(): array
    {
        return $this->task->run();
    }
}