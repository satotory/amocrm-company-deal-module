<?php

namespace App\modules\Korzilla\AmoCRM\Service;

use App\modules\Korzilla\AmoCRM\Config\AmoCRMConfig;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyCreateInput;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyEditInput;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanyGetInfoInput;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\CompanySearchInput;
use App\modules\Korzilla\AmoCRM\Data\Transporters\Inputs\DealCreateInput;
use App\modules\Korzilla\AmoCRM\Factories\CompanyCreateActionFactory;
use App\modules\Korzilla\AmoCRM\Factories\CompanyEditActionFactory;
use App\modules\Korzilla\AmoCRM\Factories\CompanyGetInfoActionFactory;
use App\modules\Korzilla\AmoCRM\Factories\CompanySearchActionFactory;
use App\modules\Korzilla\AmoCRM\Factories\DealCreateActionFactory;
use App\modules\Korzilla\AmoCRM\Factories\GetAllUsersFactory;
use App\modules\Korzilla\AmoCRM\Tasks\TokenGetNewTask;

class AmoCRMService
{
    private $config;
    public function __construct(AmoCRMConfig $config)
    {
        $this->config = $config;
        if ($this->tokenNeedToUpdate()) {
            $this->getNewAccessToken();
        }
    }

    public function companySearch(CompanySearchInput $input)
    {
        return (CompanySearchActionFactory::get($this->config))->run($input);
    }

    public function dealCreate(DealCreateInput $input)
    {
        return (DealCreateActionFactory::get($this->config))->run($input);
    }

    public function companyCreate(CompanyCreateInput $input)
    {
        return (CompanyCreateActionFactory::get($this->config))->run($input);
    }

    public function companyEdit(CompanyEditInput $input) 
    {
        return (CompanyEditActionFactory::get($this->config))->run($input);
    }

    public function getAllUsers()
    {
        return (GetAllUsersFactory::get($this->config))->run();
    }

    public function getCompanyInfo(CompanyGetInfoInput $input)
    {
        return (CompanyGetInfoActionFactory::get($this->config))->run($input);
    }

    private function tokenNeedToUpdate()
    {
        return $this->config->getTokenTimestamp() <= time();
    }

    private function getNewAccessToken()
    {
        (new TokenGetNewTask($this->config))->run();
    }
}