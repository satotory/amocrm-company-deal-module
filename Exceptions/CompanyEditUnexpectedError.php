<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class CompanyEditUnexpectedError extends CriticalException
{
    protected $message = "Company edit unexpected error";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}