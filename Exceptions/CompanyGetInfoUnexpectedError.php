<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class CompanyGetInfoUnexpectedError extends CriticalException
{
    protected $message = "Company get info unexpected error";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}