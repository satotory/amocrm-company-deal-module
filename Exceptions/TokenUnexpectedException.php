<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class TokenUnexpectedException extends CriticalException
{
    protected $message = "Token getting unexpected error";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}