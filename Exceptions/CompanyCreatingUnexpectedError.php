<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class CompanyCreatingUnexpectedError extends CriticalException
{
    protected $message = "Company creating unexpected error";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}