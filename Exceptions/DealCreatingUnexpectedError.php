<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class DealCreatingUnexpectedError extends CriticalException
{
    protected $message = "Deal creating unexpected error";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}