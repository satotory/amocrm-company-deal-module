<?php

namespace App\modules\Korzilla\AmoCRM\Exceptions;

class CompanyNotFoundException extends CriticalException
{
    protected $message = "Company Not found";

    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($this->message);
    }
}